//
//  ADMovieModel.h
//  Movie Search
//
//  Created by Preston Lewis on 3/15/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADMovieModel : NSObject
@property (nonatomic, copy) NSString* trackName;
@property (nonatomic, copy) NSString* artworkUrl;
@property (nonatomic, copy) NSString* contentAdvisoryRating;
@property (nonatomic, copy) NSDate* releaseDate;
@property (nonatomic, copy) NSString* collectionPrice;
@property (nonatomic, copy) NSString* collectionCurrency;
@property (nonatomic, copy) NSString* longDescription;
@property (nonatomic, copy) NSString* collectionViewURL;
@property (nonatomic, copy) NSString* previewURL;

-(id)initWithValues:(NSDictionary*)values;
-(NSString*)releaseDateString;

@end
