//
//  ADMovieDetailViewController.m
//  Movie Search
//
//  Created by Preston Lewis on 3/15/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import "ADMovieDetailViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "ADHTTPClient.h"

static void* ADMovieDetailViewControllerKVOContext = &ADMovieDetailViewControllerKVOContext;

@interface ADMovieDetailViewController ()
-(void)setupViews;
-(void)applyStyle;
-(void)layoutViews;
-(void)updateViewFromModel;
-(void)purchaseButtonPushed;
-(void)previewButtonPushed;
@end

@implementation ADMovieDetailViewController
@synthesize trackNameLabel;
@synthesize artworkImageView;
@synthesize contentAdvisoryLabel;
@synthesize releaseDateLabel;
@synthesize priceLabel;
@synthesize longDescriptionTextView;
@synthesize purchaseButton;
@synthesize previewButton;
@synthesize movieModel = _movieModel;

-(id)initWithMovieModel:(ADMovieModel *)movieModel
{
    self = [super init];
    if(self) {
        self.movieModel = movieModel;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupViews];
    [self layoutViews];
    [self applyStyle];
    [self updateViewFromModel];
    [self addObserver:self forKeyPath:@"movieModel"
              options:NSKeyValueObservingOptionOld
              context:ADMovieDetailViewControllerKVOContext];
    
    self.navigationItem.title = @"Details";
}

-(void)setupViews
{
    NSInteger autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
                                 UIViewAutoresizingFlexibleTopMargin |
                                 UIViewAutoresizingFlexibleWidth |
                                 UIViewAutoresizingFlexibleHeight;
    
    self.trackNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.trackNameLabel.autoresizingMask = autoresizingMask;
    [self.view addSubview:self.trackNameLabel];
    
    self.artworkImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.artworkImageView.autoresizingMask = autoresizingMask;
    [self.view addSubview:self.artworkImageView];
    
    self.contentAdvisoryLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.contentAdvisoryLabel.autoresizingMask = autoresizingMask;
    [self.view addSubview:self.contentAdvisoryLabel];
    
    self.releaseDateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.releaseDateLabel.autoresizingMask = autoresizingMask;
    [self.view addSubview:self.releaseDateLabel];
    
    self.priceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.priceLabel.autoresizingMask = autoresizingMask;
    [self.view addSubview:self.priceLabel];
    
    self.longDescriptionTextView = [[UITextView alloc] initWithFrame:CGRectZero];
    self.longDescriptionTextView.autoresizingMask = autoresizingMask;
    self.longDescriptionTextView.editable = NO;
    [self.view addSubview:self.longDescriptionTextView];
    
    self.purchaseButton = [[UIButton alloc] initWithFrame:CGRectZero];
    self.purchaseButton.autoresizingMask = autoresizingMask;
    [self.purchaseButton addTarget:self action:@selector(purchaseButtonPushed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.purchaseButton];
    
    self.previewButton = [[UIButton alloc] initWithFrame:CGRectZero];
    self.previewButton.autoresizingMask = autoresizingMask;
    [self.previewButton addTarget:self action:@selector(previewButtonPushed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.previewButton];
}

-(void)applyStyle
{
    self.trackNameLabel.backgroundColor = [UIColor clearColor];
    self.trackNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:22.0f];
    self.trackNameLabel.textAlignment = NSTextAlignmentCenter;
    
    self.artworkImageView.backgroundColor = [UIColor blackColor];
    self.artworkImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.artworkImageView.layer.borderWidth = 2.0f;
    self.artworkImageView.contentMode = UIViewContentModeCenter;
    
    self.releaseDateLabel.backgroundColor = [UIColor clearColor];
    self.releaseDateLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    self.releaseDateLabel.textAlignment = NSTextAlignmentLeft;
    
    self.contentAdvisoryLabel.backgroundColor = [UIColor clearColor];
    self.contentAdvisoryLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    self.contentAdvisoryLabel.textAlignment = NSTextAlignmentLeft;
    
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f];
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    
    [self.purchaseButton setTitle:@"Purchase" forState:UIControlStateNormal];
    [self.purchaseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.purchaseButton.backgroundColor = [UIColor colorWithRed:0.3f green:0.6f blue:0.4f alpha:1.0f];
    self.purchaseButton.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    self.purchaseButton.layer.borderWidth = 1.0f;
    self.purchaseButton.layer.cornerRadius = 6.0f;
    self.purchaseButton.clipsToBounds = YES;
    
    [self.previewButton setTitle:@"Preview" forState:UIControlStateNormal];
    [self.previewButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.previewButton.backgroundColor = [UIColor colorWithRed:0.3f green:0.4f blue:0.6f alpha:1.0f];
    self.previewButton.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    self.previewButton.layer.borderWidth = 1.0f;
    self.previewButton.layer.cornerRadius = 6.0f;
    self.previewButton.clipsToBounds = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void)layoutViews
{
    CGFloat y = CGRectGetMaxY(self.navigationController.navigationBar.frame) + 5.0f;
    self.trackNameLabel.frame = CGRectMake(10.0f, y, self.view.frame.size.width - 20.0f, 34.0f);
    y = CGRectGetMaxY(self.trackNameLabel.frame) + 5.0f;
    
    self.artworkImageView.frame = CGRectMake(10.0f, y, 100.0f, 100.0f);
    
    CGFloat x = CGRectGetMaxX(self.artworkImageView.frame)+10.0f;
    y -= 3.0f;
    self.releaseDateLabel.frame = CGRectMake(x, y, self.view.frame.size.width - x - 10.0f, 22.0f);
    y = CGRectGetMaxY(self.releaseDateLabel.frame);
    
    self.contentAdvisoryLabel.frame = CGRectMake(x, y, self.view.frame.size.width - x - 10.0f, 22.0f);
    y = CGRectGetMaxY(self.contentAdvisoryLabel.frame);
    
    self.priceLabel.frame = CGRectMake(x, y, self.view.frame.size.width - x - 10.0f, 22.0f);
    y = CGRectGetMaxY(self.priceLabel.frame);
    
    self.purchaseButton.frame = CGRectMake(x, y, (self.view.frame.size.width - x)/2 - 13.0f, 34.0f);
    x = CGRectGetMaxX(self.purchaseButton.frame) + 6.0f;
    
    self.previewButton.frame = CGRectMake(x, y, self.view.frame.size.width - x - 13.0f, 34.0f);
    y = CGRectGetMaxY(self.artworkImageView.frame);
    
    self.longDescriptionTextView.frame = CGRectMake(10.0f, y, self.view.frame.size.width - 20.0f, self.view.frame.size.height - y);
}

-(void)updateViewFromModel
{
    self.trackNameLabel.text = self.movieModel.trackName;
    
    [[ADHTTPClient sharedClient] imageForURL:self.movieModel.artworkUrl
                                     success:^(UIImage *image) {
                                         self.artworkImageView.image = image;
                                     }
                                     failure:nil];
    
    self.releaseDateLabel.text = [NSString stringWithFormat:@"Released: %@", [self.movieModel releaseDateString]];
    
    if(self.movieModel.contentAdvisoryRating.length > 0) {
        self.contentAdvisoryLabel.text = [NSString stringWithFormat:@"Rating: %@", self.movieModel.contentAdvisoryRating];
    }
    else {
        self.contentAdvisoryLabel.text = [NSString stringWithFormat:@"Rating: NA"];
    }
    
    self.priceLabel.text = [NSString stringWithFormat:@"Price: %@ %@", self.movieModel.collectionPrice, self.movieModel.collectionCurrency];
    
    
    if(self.movieModel.longDescription.length > 0) {
        self.longDescriptionTextView.text = self.movieModel.longDescription;
    }
    else {
        self.longDescriptionTextView.text = @"No description.";
    }
}

-(void)purchaseButtonPushed
{
    NSLog(@"Launching iTunes link: %@", self.movieModel.collectionViewURL);
    NSURL* url = [NSURL URLWithString:self.movieModel.collectionViewURL];
    [[UIApplication sharedApplication] openURL:url];
}

-(void)previewButtonPushed
{
    NSLog(@"Playing preview: %@", self.movieModel.previewURL);
    NSURL* url = [NSURL URLWithString:self.movieModel.previewURL];
    [[UIApplication sharedApplication] openURL:url];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if(context == ADMovieDetailViewControllerKVOContext) {
        if([keyPath isEqualToString:@"movieModel"]) {
            [self updateViewFromModel];
        }
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self layoutViews];
}

-(void)dealloc
{
    [self removeObserver:self forKeyPath:@"movieModel"];
}

@end
