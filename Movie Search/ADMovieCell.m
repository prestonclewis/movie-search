//
//  ADMovieCell.m
//  Movie Search
//
//  Created by Preston Lewis on 3/16/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import "ADMovieCell.h"
#import "ADHTTPClient.h"

static void* ADMovieCellKVOContext = &ADMovieCellKVOContext;
static const CGFloat kADMovieCellRecommendedHeight = 104.0f;

@implementation ADMovieCell
@synthesize artworkImageView;
@synthesize trackNameLabel;
@synthesize contentAdvisoryLabel;
@synthesize movieModel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.artworkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2.0f, 2.0f, 100.0f, 100.0f)];
        self.artworkImageView.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:self.artworkImageView];
        
        self.trackNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(106.0f, 2.0f, self.frame.size.width - 106.0f, 66.0f)];
        self.trackNameLabel.numberOfLines = 2;
        self.trackNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.trackNameLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.contentView addSubview:self.trackNameLabel];
        
        self.contentAdvisoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(106.0f, 66.0f, self.frame.size.width - 106.0f, 34.0f)];
        self.contentAdvisoryLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.contentAdvisoryLabel.textAlignment = NSTextAlignmentLeft;
        self.contentAdvisoryLabel.textColor = [UIColor redColor];
        [self.contentView addSubview:self.contentAdvisoryLabel];
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        [self addObserver:self forKeyPath:@"movieModel" options:NSKeyValueObservingOptionOld context:ADMovieCellKVOContext];
    }
    return self;
}

-(void)prepareForReuse
{
    self.artworkImageView.image = nil;
    self.trackNameLabel.text = @"";
    self.contentAdvisoryLabel.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateView
{
    self.trackNameLabel.text = self.movieModel.trackName;
    if(self.movieModel.contentAdvisoryRating.length > 0) {
        self.contentAdvisoryLabel.text = [NSString stringWithFormat:@"Rating: %@", self.movieModel.contentAdvisoryRating];
        self.contentAdvisoryLabel.hidden = NO;
    }
    else {
        self.contentAdvisoryLabel.hidden = YES;
    }
    
    [[ADHTTPClient sharedClient] imageForURL:self.movieModel.artworkUrl
                                     success:^(UIImage *image) {
                                         self.artworkImageView.image = image;
                                     }
                                     failure:nil];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if(context == ADMovieCellKVOContext) {
        if([keyPath isEqualToString:@"movieModel"]) {
            [self updateView];
        }
    }
}

+(CGFloat)recommendedHeight
{
    return kADMovieCellRecommendedHeight;
}

-(void)dealloc
{
    [self removeObserver:self forKeyPath:@"movieModel"];
}

@end
