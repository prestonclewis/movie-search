//
//  ADMovieListViewController.h
//  Movie Search
//
//  Created by Preston Lewis on 3/15/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADMovieListViewController : UIViewController

@property (nonatomic, retain) UISearchBar* searchBar;
@property (nonatomic, retain) UITableView* tableView;
@property (nonatomic, retain) NSArray* movieModels;
@property (readonly) NSString* currentSearchTerm;

@end
