//
//  ADMovieCell.h
//  Movie Search
//
//  Created by Preston Lewis on 3/16/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADMovieModel.h"

@interface ADMovieCell : UITableViewCell
@property (nonatomic, retain) UIImageView* artworkImageView;
@property (nonatomic, retain) UILabel* trackNameLabel;
@property (nonatomic, retain) UILabel* contentAdvisoryLabel;
@property (nonatomic, retain) ADMovieModel* movieModel;

+(CGFloat)recommendedHeight;

@end
