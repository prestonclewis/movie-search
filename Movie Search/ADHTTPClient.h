//
//  ADHTTPClient.h
//  Movie Search
//
//  Created by Preston Lewis on 3/15/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ADHTTPClientSuccess)(NSArray* movies);
typedef void (^ADHTTPClientFailure)(NSError* error);
typedef void (^ADHTTPClientImageSuccess)(UIImage* image);

@interface ADHTTPClient : NSObject

@property (nonatomic, retain) NSOperationQueue* operationQueue;
@property (nonatomic, retain) NSCache* imageCache;

-(void)searchMoviesForString:(NSString*)searchTerm
                     success:(ADHTTPClientSuccess)success
                     failure:(ADHTTPClientFailure)failure;

+(ADHTTPClient*)sharedClient;

-(void)imageForURL:(NSString*)imageURL
           success:(ADHTTPClientImageSuccess)success
           failure:(ADHTTPClientFailure)failure;

@end
