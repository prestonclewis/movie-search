//
//  ADMovieModel.m
//  Movie Search
//
//  Created by Preston Lewis on 3/15/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import "ADMovieModel.h"

// Constants for API response keys -- let's us change them easily in one place
// if API spec changes in the future.
static NSString* const kResponseKeyTrackName                = @"trackName";
static NSString* const kResponseKeyArtworkUrl               = @"artworkUrl100";
static NSString* const kResponseKeyContentAdvisoryRating    = @"contentAdvisoryRating";
static NSString* const kResponseKeyReleaseDate              = @"releaseDate";
static NSString* const kResponseKeyCollectionPrice          = @"collectionPrice";
static NSString* const kResponseKeyCollectionPriceCurrency  = @"currency";
static NSString* const kResponseKeyLongDescription          = @"longDescription";
static NSString* const kResponseKeyCollectionViewUrl        = @"trackViewUrl";
static NSString* const kResponseKeyPreviewUrl               = @"previewUrl";

@interface ADMovieModel ()
-(NSDate*)convertDate:(NSString*)string;
@end

@implementation ADMovieModel
@synthesize trackName;
@synthesize artworkUrl;
@synthesize contentAdvisoryRating;
@synthesize releaseDate;
@synthesize collectionPrice;
@synthesize collectionCurrency;
@synthesize longDescription;
@synthesize collectionViewURL;
@synthesize previewURL;

-(id)initWithValues:(NSDictionary*)values
{
    self = [super init];
    if(self) {
        self.trackName = values[kResponseKeyTrackName];
        self.artworkUrl = values[kResponseKeyArtworkUrl];
        self.contentAdvisoryRating = values[kResponseKeyContentAdvisoryRating];
        self.releaseDate = [self convertDate:values[kResponseKeyReleaseDate]];
        self.collectionPrice = values[kResponseKeyCollectionPrice];
        self.collectionCurrency = values[kResponseKeyCollectionPriceCurrency];
        self.longDescription = values[kResponseKeyLongDescription];
        self.collectionViewURL = values[kResponseKeyCollectionViewUrl];
        
        if([values[kResponseKeyPreviewUrl] isKindOfClass:[NSArray class]]) {
            if([values[kResponseKeyPreviewUrl] count] > 0) {
                self.previewURL = [values[kResponseKeyPreviewUrl] objectAtIndex:0];
            }
        }
        else {
            self.previewURL = values[kResponseKeyPreviewUrl];
        }
    }
    return self;
}

-(NSDate*)convertDate:(NSString *)string
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";
    NSDate* date = [formatter dateFromString:string];
    return date;
}

-(NSString*)releaseDateString
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MM'/'dd'/'yyyy";
    NSString* dateString = [formatter stringFromDate:self.releaseDate];
    return dateString;
}

-(NSString*)description
{
    NSMutableString* desc = [[NSMutableString alloc] init];
    [desc appendFormat:@"<%@: %p %@>", [self class], self, self.trackName];
    return desc;
}

@end
