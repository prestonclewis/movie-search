//
//  ADMovieDetailViewController.h
//  Movie Search
//
//  Created by Preston Lewis on 3/15/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADMovieModel.h"

@interface ADMovieDetailViewController : UIViewController

@property (nonatomic, retain) UILabel* trackNameLabel;
@property (nonatomic, retain) UIImageView* artworkImageView;
@property (nonatomic, retain) UILabel* contentAdvisoryLabel;
@property (nonatomic, retain) UILabel* releaseDateLabel;
@property (nonatomic, retain) UILabel* priceLabel;
@property (nonatomic, retain) UITextView* longDescriptionTextView;
@property (nonatomic, retain) UIButton* purchaseButton;
@property (nonatomic, retain) UIButton* previewButton;
@property (nonatomic, retain) ADMovieModel* movieModel;


-(id)initWithMovieModel:(ADMovieModel*)movieModel;

@end
