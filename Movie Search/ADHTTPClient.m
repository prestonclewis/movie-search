//
//  ADHTTPClient.m
//  Movie Search
//
//  Created by Preston Lewis on 3/15/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import "ADHTTPClient.h"
#import <CFNetwork/CFNetwork.h>
#import "ADMovieModel.h"

static NSString* const kSearchBaseURL = @"https://itunes.apple.com/search";
static NSString* const kResponseKeyResultCount  = @"resultCount";
static NSString* const kResponseKeyResults      = @"results";

static ADHTTPClient* ADHTTPClientSingleton = nil;

@interface ADHTTPClient ()
-(NSArray*)movieModelsFromResponseData:(NSDictionary*)responseData;
-(NSString*)getQueryString:(NSDictionary*)parameters;
-(NSString*)urlEncode:(NSString*)string;
@end

@interface ADHTTPClient (NSUrlConnectionDelegate) <NSURLConnectionDataDelegate, NSURLConnectionDelegate, NSURLConnectionDownloadDelegate>
@end

@implementation ADHTTPClient
@synthesize operationQueue;
@synthesize imageCache;

-(id)init
{
    self = [super init];
    if(self) {
        self.operationQueue = [[NSOperationQueue alloc] init];
        self.imageCache = [[NSCache alloc] init];
    }
    return self;
}

-(void)searchMoviesForString:(NSString*)searchTerm
                     success:(ADHTTPClientSuccess)success
                     failure:(ADHTTPClientFailure)failure
{
    //Build Parameters
    NSString* encodedSearchTerm = [self urlEncode:searchTerm];
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setValue:encodedSearchTerm forKey:@"term"];
    [params setValue:@"movie" forKey:@"media"];
    
    //Build HTTP Request URL
    NSString* queryString = [self getQueryString:params];
    NSString* urlString = [NSString stringWithFormat:@"%@%@", kSearchBaseURL, queryString];
    NSURL* url = [NSURL URLWithString:urlString];
    
    //Get dispatch qeuue for async request
    dispatch_queue_t bgQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, (unsigned long)NULL);
    
    //Log request
    NSLog(@"GET - %@", urlString);
    
    //Dispatch request
    dispatch_async(bgQueue, ^{
        //Parse response data
        NSData* responseData = [NSData dataWithContentsOfURL:url];
        NSError* error = nil;
        id object = [NSJSONSerialization JSONObjectWithData:responseData
                                                    options:0
                                                      error:&error];
        
        if(!error) {
            if([object isKindOfClass:[NSDictionary class]]) {
                //Build Movies Objects
                NSArray* movieModels = [self movieModelsFromResponseData:(NSDictionary*)object];
                
                //Fire success callback from main queue
                dispatch_async(dispatch_get_main_queue(), ^{
                    success(movieModels);
                });
            }
        }
        else {
            NSLog(@"Error parsing JSON: %@", error);
            
            //Fire failure callback from main queue
            dispatch_async(dispatch_get_main_queue(), ^{
                failure(error);
            });
        }
    });
}

-(NSArray*)movieModelsFromResponseData:(NSDictionary*)responseData
{
    NSUInteger resultCount = [responseData[kResponseKeyResultCount] unsignedIntegerValue];
    NSMutableArray* movieModels = [[NSMutableArray alloc] initWithCapacity:resultCount];
    NSArray* resultsData = responseData[kResponseKeyResults];
    for(NSDictionary* resultDict in resultsData) {
        ADMovieModel* movieModel = [[ADMovieModel alloc] initWithValues:resultDict];
        [movieModels addObject:movieModel];
    }
    return movieModels;
}

-(NSString*)getQueryString:(NSDictionary*)parameters
{
    NSMutableString* queryString = [[NSMutableString alloc] init];
    [queryString appendFormat:@"?"];
    for(NSString* key in [parameters allKeys]) {
        [queryString appendFormat:@"%@=%@&", key, parameters[key]];
    }
    return [queryString substringToIndex:queryString.length - 1];
}

+(ADHTTPClient*)sharedClient
{
    if(!ADHTTPClientSingleton) {
        ADHTTPClientSingleton = [[ADHTTPClient alloc] init];
    }
    return ADHTTPClientSingleton;
}

-(void)imageForURL:(NSString*)imageURL
           success:(ADHTTPClientImageSuccess)success
           failure:(ADHTTPClientFailure)failure
{
    //Check image cache
    if([self.imageCache objectForKey:imageURL]) {
        success([self.imageCache objectForKey:imageURL]);
    }
    
    //Otherwise fetch image
    dispatch_queue_t dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatchQueue, ^{
        NSData* imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
        if(!imageData) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //Error
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                details[NSLocalizedDescriptionKey] = [NSString stringWithFormat: @"Error fetching image for %@", imageURL];
                NSError* error = [NSError errorWithDomain:@"ad" code:200 userInfo:details];
                if(failure != nil) {
                    failure(error);
                }
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                //Success
                UIImage* image = [UIImage imageWithData:imageData];
                [self.imageCache setObject:image forKey:imageURL];
                if(success != nil) {
                    success(image);
                }
            });
        }
    });
}

-(NSString*)urlEncode:(NSString*)string
{
    NSString *charactersToEscape = @"!*'();:@&=+$,/?%#[]\" ";
    NSCharacterSet *allowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:charactersToEscape] invertedSet];
    NSString *encodedString = [string stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    return encodedString;
}

@end
