//
//  ADMovieListViewController.m
//  Movie Search
//
//  Created by Preston Lewis on 3/15/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import "ADMovieListViewController.h"
#import "ADMovieCell.h"
#import "ADHTTPClient.h"
#import "ADMovieDetailViewController.h"

@interface ADMovieListViewController ()
-(void)search;
@end

@interface ADMovieListViewController (UISearchBarDelegate) <UISearchBarDelegate>
@end

@interface ADMovieListViewController (UITableViewDelegate) <UITableViewDataSource, UITableViewDelegate>
@end


@implementation ADMovieListViewController
@synthesize searchBar = _searchBar;
@synthesize tableView = _tableView;
@synthesize movieModels = _movieModels;
@synthesize currentSearchTerm = _currentSearchTerm;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Search";

    //Search bar
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 300.0f, 34.0f)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = @"Search for movies";
    self.navigationItem.titleView = self.searchBar;
    
    //Table view
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)
                                                  style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)search
{
    UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = self.tableView.center;
    [self.view addSubview:spinner];
    [spinner startAnimating];
    [[ADHTTPClient sharedClient] searchMoviesForString:self.searchBar.text
                                               success:^(NSArray *movies) {
                                                   self.movieModels = movies;
                                                   [self.tableView reloadData];
                                                   [spinner stopAnimating];
                                                   [spinner removeFromSuperview];
                                               }
                                               failure:^(NSError *error) {
                                                   [self showConnectionAlert];
                                                   [spinner stopAnimating];
                                                   [spinner removeFromSuperview];
                                               }];
    _currentSearchTerm = [self.searchBar.text copy];
}

-(void)showConnectionAlert
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue"
                                                    message:@"Search could not be completed. Please check your connection and try again."
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"Ok", nil];
    [alert show];
}

@end

#pragma mark - UISearchBar Delegate
@implementation ADMovieListViewController (UISearchBarDelegate)

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self search];
    [searchBar resignFirstResponder];
}

@end

#pragma mark - UITableView Delegate/DataSource
@implementation ADMovieListViewController (UITableViewDelegate)
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.movieModels count];
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(self.currentSearchTerm) {
        return [NSString stringWithFormat:@"%lu Results for '%@'", [self.movieModels count], self.currentSearchTerm];
    }
    else {
        return [NSString stringWithFormat:@"%lu Results", [self.movieModels count]];
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"MovieCell";
    ADMovieCell* cell = (ADMovieCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell) {
        cell = [[ADMovieCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.movieModel = self.movieModels[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADMovieModel* selectedMovie = self.movieModels[indexPath.row];
    ADMovieDetailViewController* movieDetailVC = [[ADMovieDetailViewController alloc] initWithMovieModel:selectedMovie];
    [self.navigationController pushViewController:movieDetailVC animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [ADMovieCell recommendedHeight];
}

@end
