//
//  ADAppDelegate.h
//  Movie Search
//
//  Created by Preston Lewis on 3/15/14.
//  Copyright (c) 2014 Preston Lewis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
